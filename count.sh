#!/bin/sh

if test "$#" -lt 2
then echo "Usage: $0 <directory> <output_file>\n"
fi

total=0
cnt=0

cd $1

for entry in `ls *.txt`
do
    cnt=&(wc -c <  "$entry")
    total=$total+$cnt
    echo "$entry: $cnt" >> $2
    echo "$entry: $cnt"
done

    echo "TOTAL: $total" >> $2



