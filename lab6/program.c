#include <errno.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <unistd.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>



int countChars(char buffer[])
{
    int i = 0;
    {
        while(buffer[i]!= '\0')
        i++;
    }
    return i;
}

 int main(int argc, char* argv[])
{

    struct stat st;
    int Upper = 0;
    int Lower = 0;
    int Digit = 0;
    int Specific_char = 0;
    int File_size = 0;
    char buffer[4096];
    char c;
    int sr = 0;
    if(argc != 4)
    {
    printf("Usage error: ./program <fisier_intrare> <fisier_statistica> <ch>\n");
    exit(-1);
    }

    int File_in = open(argv[1], O_RDONLY); 

    if (File_in < 0) 
    { 
        perror("c1"); 
        exit(1); 
    } 

    if (stat(argv[1], &st) ==-1)
        return -1;
    File_size  = st.st_size;

    int File_out = open(argv[2], O_WRONLY | O_APPEND);

    if (File_in < 0) 
    { 
        perror("c1"); 
        exit(1); 
    } 

    while(read(File_in, &c, 1))
    {
        if(islower(c))
            Lower++;
        if(isupper(c))
            Upper++;
        if(isdigit(c))
            Digit++;
        if(c == argv[3][0])
            Specific_char++;
    }

        sprintf(buffer, "numar litere mici: %d\n", Lower);
        sr = write(File_out, buffer, countChars(buffer));
        sprintf(buffer, "numar litere mari: %d\n", Upper);
        sr = write(File_out, buffer, countChars(buffer));
        sprintf(buffer, "numar cifre: %d\n", Digit);
        sr = write(File_out, buffer, countChars(buffer));
        sprintf(buffer, "numar aparitii caracter: %d\n", Specific_char);
        sr = write(File_out, buffer, countChars(buffer));
        sprintf(buffer, "dimensiune fisier: %d\n", File_size);
        sr = write(File_out, buffer, countChars(buffer));

        if (close(File_in) < 0) 
        { 
            perror("c1"); 
            exit(1); 
        }

        if (close(File_out < 0)) 
        { 
            perror("c1"); 
            exit(1);
        } 
}
