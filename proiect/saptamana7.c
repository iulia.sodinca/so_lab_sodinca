#include <errno.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <unistd.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

char buffer[4096];

const char *get_filename_ext(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

 char* getUserRights(mode_t mode)
{
    char* rights = malloc(4);
    strcpy(rights, "---");
    if (rights == NULL) {
        exit(EXIT_FAILURE);
    }
    if(mode & S_IRUSR)
    {
        rights[0] = 'R';
    }
    if(mode & S_IWUSR)
    {
        rights[1] = 'W';
    }
    if(mode & S_IXUSR)
    {
        rights[2] = 'X';
    }
    return rights;
}

char* getGroupRights(mode_t mode)
{
    char* rights = malloc(4);
    strcpy(rights, "---");
    if (rights == NULL) {
        exit(EXIT_FAILURE);
    }
    if(mode & S_IRGRP)
    {
        rights[0] = 'R';
    }
    if(mode & S_IWGRP)
    {
        rights[1] = 'W';
    }
    if(mode & S_IXGRP)
    {
        rights[2] = 'X';
    }
    return rights;
}

char* getOtherRights(mode_t mode)
{
    char* rights = malloc(4);
    strcpy(rights, "---");
    if (rights == NULL) {
        exit(EXIT_FAILURE);
    }
    if(mode & S_IROTH)
    {
        rights[0] = 'R';
    }
    if(mode & S_IWOTH)
    {
        rights[1] = 'W';
    }
    if(mode & S_IXOTH)
    {
        rights[2] = 'X';
    }
    return rights;
}

int countChars(char buffer[])
{
    int i = 0;
    while(buffer[i]!= '\0')
    {
        i++;
    }
    return i;
}

void writeLinkName(char* Name)
{
        sprintf(buffer, "nume legatura: %s\n", Name);
}

void writeDirectoryName(char* Name)
{
        sprintf(buffer, "nume director: %s\n", Name);
}

void writeFileName(char* Name)
{
        sprintf(buffer, "nume fisier: %s\n", Name);
}

void writeFileHeigth(int Heigth)
{
    sprintf(buffer, "inaltime: %d\n", Heigth);
}

void writeFileWidth(int Width)
{
    sprintf(buffer, "lungime: %d\n", Width);
}

void writeFileSize(int File_size)
{
   sprintf(buffer, "dimensiune fisier: %d octeti\n", File_size);
}

void writeLinkSize(int Link_size)
{
   sprintf(buffer, "dimensiune: %d octeti\n", Link_size);
}

void writeUserID(int User_id)
{ 
    sprintf(buffer, "identificatorul utilizatorului: %d\n", User_id);
}

void writeLastModified(char* Last_modified)
{
  sprintf(buffer, "timpul ultimei modificari: %s", Last_modified);
}

void writeLinkCounter(int Link_cnt)
{   
    sprintf(buffer, "contorul de legaturi: %d\n", Link_cnt);
}

void writeUserRights(char* User_rights)
{ 
    sprintf(buffer, "drepturi de acces user: %s\n", User_rights);
}

void writeGroupRights(char* Group_rights)
{
    sprintf(buffer, "drepturi de acces grup: %s\n", Group_rights);
}

void writeOtherRights(char* Other_rights)
{ 
    sprintf(buffer, "drepturi de acces altii: %s\n", Other_rights);
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        printf("Usage error: ./program <director_intrare> \n");
        exit(-1);
    }

    int File_in = open(argv[1], O_RDONLY); 

    if (File_in < 0) 
    { 
        perror("eroare deschidere fisier argument"); 
        exit(1); 
    } 

    int File_out = open("statistica.txt", O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);

    if (File_out < 0) 
    { 
        perror("eroare deschidere fisier statistica"); 
        exit(1); 
    } 

    struct stat st;
    if (stat(argv[1], &st) == -1)
        return -1;

      struct stat lst;
        if (lstat(argv[1], &lst) == -1)
        return -1;

        int Link_size = lst.st_size;

    int User_id = st.st_ino;
    int File_size = st.st_size;
    char* Last_modified = ctime(&st.st_mtime);
    int Link_cnt = st.st_nlink;
    char* User_rights = getUserRights(st.st_mode);
    char* Group_rights = getGroupRights(st.st_mode);
    char* Other_rights = getOtherRights(st.st_mode);
    char* Name = argv[1];
    int sr;

    if(S_ISREG(st.st_mode))
    {
      writeFileName(Name);
      sr = write(File_out, buffer, countChars(buffer));
      if(!strcmp(get_filename_ext(argv[1]), "bmp"))
	{
	  lseek(File_in, 18, SEEK_SET);
	  int Heigth = 0;
	  read(File_in, &Heigth, sizeof(int));

	  lseek(File_in, 22, SEEK_SET);
	  int Width = 0;
	  read(File_in, &Width, sizeof(int));
          writeFileHeigth(Heigth);
	  sr = write(File_out, buffer, countChars(buffer));
	  writeFileWidth(Width);
	  sr = write(File_out, buffer, countChars(buffer));
	}
    writeFileSize(File_size);
    sr = write(File_out, buffer, countChars(buffer));
    writeUserID(User_id);
    sr = write(File_out, buffer, countChars(buffer));
    writeLastModified(Last_modified);
    sr = write(File_out, buffer, countChars(buffer));
    writeLinkCounter(Link_cnt);
    sr = write(File_out, buffer, countChars(buffer));
    writeUserRights(User_rights);
    sr = write(File_out, buffer, countChars(buffer));
    writeGroupRights(Group_rights);
    sr = write(File_out, buffer, countChars(buffer));
    writeOtherRights(Other_rights);
    sr = write(File_out, buffer, countChars(buffer));
    }

    if(S_ISLNK(lst.st_mode))
    {

        writeLinkName(Name);
        sr = write(File_out, buffer, countChars(buffer));
        writeLinkSize(Link_size);
        sr = write(File_out, buffer, countChars(buffer));
        writeFileSize(File_size);
        sr = write(File_out, buffer, countChars(buffer));
        writeUserRights(User_rights);
        sr = write(File_out, buffer, countChars(buffer));
        writeGroupRights(Group_rights);
        sr = write(File_out, buffer, countChars(buffer));
        writeOtherRights(Other_rights);
        sr = write(File_out, buffer, countChars(buffer));

    }

    if(S_ISDIR(st.st_mode))
    {
        writeDirectoryName(Name);
        sr = write(File_out, buffer, countChars(buffer));
        writeUserID(User_id);
        sr = write(File_out, buffer, countChars(buffer));
        writeFileSize(File_size);
        sr = write(File_out, buffer, countChars(buffer));
        writeUserRights(User_rights);
        sr = write(File_out, buffer, countChars(buffer));
        writeGroupRights(Group_rights);
        sr = write(File_out, buffer, countChars(buffer));
        writeOtherRights(Other_rights);
        sr = write(File_out, buffer, countChars(buffer));
    }

    if (close(File_in) < 0) 
    { 
        perror("eroare inchidere fisier argument"); 
        exit(1); 
    }

    if (close(File_out) < 0) 
    { 
        perror("eroare inchidere fisier statistica"); 
        exit(1);
    } 
}
