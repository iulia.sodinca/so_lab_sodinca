#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>

char buffer[4096];

char *GetFileName(char *Name)
{
	char *File_Name = malloc(sizeof(Name) + 100);
	strcpy(File_Name, Name);
	strcat(File_Name, "_statistica.txt");
	return File_Name;
}

char *GetFullPath(char *Directory, char *Name)
{
	char *Path = malloc(sizeof(Name) + sizeof(Directory) + 60);
	strcpy(Path, Directory);
	strcat(Path, "/");
	strcat(Path, Name);
	return Path;
}

const char *get_filename_ext(const char *filename)
{
	const char *dot = strrchr(filename, '.');
	if (!dot || dot == filename)
		return "";
	return dot + 1;
}

char *getUserRights(mode_t mode)
{
	char *rights = malloc(4);
	strcpy(rights, "---");
	if (rights == NULL)
	{
		exit(EXIT_FAILURE);
	}
	if (mode & S_IRUSR)
	{
		rights[0] = 'R';
	}
	if (mode & S_IWUSR)
	{
		rights[1] = 'W';
	}
	if (mode & S_IXUSR)
	{
		rights[2] = 'X';
	}
	return rights;
}

char *getGroupRights(mode_t mode)
{
	char *rights = malloc(4);
	strcpy(rights, "---");
	if (rights == NULL)
	{
		exit(EXIT_FAILURE);
	}
	if (mode & S_IRGRP)
	{
		rights[0] = 'R';
	}
	if (mode & S_IWGRP)
	{
		rights[1] = 'W';
	}
	if (mode & S_IXGRP)
	{
		rights[2] = 'X';
	}
	return rights;
}

char *getOtherRights(mode_t mode)
{
	char *rights = malloc(4);
	strcpy(rights, "---");
	if (rights == NULL)
	{
		exit(EXIT_FAILURE);
	}
	if (mode & S_IROTH)
	{
		rights[0] = 'R';
	}
	if (mode & S_IWOTH)
	{
		rights[1] = 'W';
	}
	if (mode & S_IXOTH)
	{
		rights[2] = 'X';
	}
	return rights;
}

int countChars(char buffer[])
{
	int i = 0;
	while (buffer[i] != '\0')
	{
		i++;
	}
	return i;
}

void writeLinkName(char *Name)
{
	sprintf(buffer, "nume legatura: %s\n", Name);
}

void writeDirectoryName(char *Name)
{
	sprintf(buffer, "nume director: %s\n", Name);
}

void writeFileName(char *Name)
{
	sprintf(buffer, "nume fisier: %s\n", Name);
}

void writeFileHeigth(int Heigth)
{
	sprintf(buffer, "inaltime: %d\n", Heigth);
}

void writeFileWidth(int Width)
{
	sprintf(buffer, "lungime: %d\n", Width);
}

void writeFileSize(int File_size)
{
	sprintf(buffer, "dimensiune fisier: %d octeti\n", File_size);
}

void writeLinkSize(int Link_size)
{
	sprintf(buffer, "dimensiune: %d octeti\n", Link_size);
}

void writeUserID(int User_id)
{
	sprintf(buffer, "identificatorul utilizatorului: %d\n", User_id);
}

void writeLastModified(char *Last_modified)
{
	sprintf(buffer, "timpul ultimei modificari: %s", Last_modified);
}

void writeLinkCounter(int Link_cnt)
{
	sprintf(buffer, "contorul de legaturi: %d\n", Link_cnt);
}

void writeUserRights(char *User_rights)
{
	sprintf(buffer, "drepturi de acces user: %s\n", User_rights);
}

void writeGroupRights(char *Group_rights)
{
	sprintf(buffer, "drepturi de acces grup: %s\n", Group_rights);
}

void writeOtherRights(char *Other_rights)
{
	sprintf(buffer, "drepturi de acces altii: %s\n", Other_rights);
}

int main(int argc, char *argv[])
{
	int w = 0;
	int status = 0;
	int Process_Counter = 0;
	if (argc != 4)
	{
		printf("Usage error: ./program <director_intrare> <director_iesire> <caracter> \n");
		exit(-1);
	}

	DIR *Input_Dir = opendir(argv[1]);
	DIR *Output_Dir = opendir(argv[2]);

	if (Input_Dir == NULL)
	{
		perror("Eroare deschidere director argument");
		exit(1);
	}

	if (Output_Dir == NULL)
	{
		perror("Eroare deschidere director statistica");
		exit(1);
	}

	if (sizeof(argv[3]) != 8)
	{
		printf("Al treilea argument tb sa fie un singur caracter\n");
		exit(1);
	}

	struct dirent *Dir_Entry = NULL;

	while ((Dir_Entry = readdir(Input_Dir)) != NULL)
	{

		char *Name = Dir_Entry->d_name;

		int sr;
		char FullPath[PATH_MAX];
		sprintf(FullPath, "%s/%s", argv[1], Name);

		struct stat st;
		if (stat(FullPath, &st) == -1)
		{
			printf("Eroare stat!\n");
			return -1;
		}

		struct stat lst;
		if (lstat(FullPath, &lst) == -1)
		{
			printf("Eroare lstat!\n");
			return -1;
		}

		int Link_size = lst.st_size;

		int User_id = st.st_ino;
		int File_size = st.st_size;
		char *Last_modified = ctime(&st.st_mtime);
		int Link_cnt = st.st_nlink;
		char *User_rights = getUserRights(st.st_mode);
		char *Group_rights = getGroupRights(st.st_mode);
		char *Other_rights = getOtherRights(st.st_mode);

		if (S_ISREG(st.st_mode))
		{
			int File_Descriptor = open(FullPath, O_RDWR);
			if (File_Descriptor == -1)
			{
				perror("Error opening file");
				exit(-1);
			}

			if (!strcmp(get_filename_ext(Name), "bmp"))
			{

				pid_t pid1 = fork();
				lseek(File_Descriptor, 18, SEEK_SET);
				int Heigth = 0;
				read(File_Descriptor, &Heigth, sizeof(int));

				lseek(File_Descriptor, 22, SEEK_SET);
				int Width = 0;
				read(File_Descriptor, &Width, sizeof(int));

				Process_Counter++;

				if (pid1 == 0)
				{
					char *File_Name = GetFileName(GetFullPath(argv[2], Name));
					int File_Stat = open(File_Name, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
					if (File_Stat == -1)
					{
						perror("Eroare deschidere fisier");
						exit(-1);
					}

					writeFileName(Name);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeFileHeigth(Heigth);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeFileWidth(Width);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeFileSize(File_size);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeUserID(User_id);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeLastModified(Last_modified);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeLinkCounter(Link_cnt);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeUserRights(User_rights);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeGroupRights(Group_rights);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeOtherRights(Other_rights);
					sr = write(File_Stat, buffer, countChars(buffer));
					if (close(File_Stat) < 0)
					{
						perror("Eroare inchidere fisier");
						exit(-1);
					}
					exit(10);
				}

				pid_t pid2 = fork();
				Process_Counter++;

				if (pid2 == 0)
				{

					lseek(File_Descriptor, 28, SEEK_SET);
					int BitCount;
					read(File_Descriptor, &BitCount, sizeof(BitCount));
					lseek(File_Descriptor, 54, SEEK_SET);

					if (BitCount <= 8) // daca avem ColorTable
					{
						int NumPixels = 1;
						for (int k = 0; k < BitCount; k++)
						{
							NumPixels *= 2; // = Number of colors
						}
						for (int i = 0; i <= NumPixels; i++)
						{
							unsigned char Pixel[4];
							read(File_Descriptor, Pixel, sizeof(Pixel));
							lseek(File_Descriptor, -4, SEEK_CUR);														  // se ia in considerare padding-ul fiecarui bit
																														  // double pt o calitate mai buna a conversiei
							double Gray = 0.299 * (double)Pixel[0] + 0.587 * (double)Pixel[1] + 0.114 * (double)Pixel[2]; // ordine biti: RGB
							unsigned char GrayPixel[4] = {(unsigned char)Gray, (unsigned char)Gray, (unsigned char)Gray, 0};
							write(File_Descriptor, GrayPixel, sizeof(GrayPixel));
						}
					}
					else // Convertim direct imaginea pixel cu pixel
					{
						for (int i = 0; i < Heigth; i++)
						{
							for (int j = 0; j < Width; j++)
							{
								unsigned char Pixel[3];
								read(File_Descriptor, Pixel, sizeof(Pixel));
								lseek(File_Descriptor, -3, SEEK_CUR); // nu mai avem padding

								double Gray = 0.299 * (double)Pixel[0] + 0.587 * (double)Pixel[1] + 0.114 * (double)Pixel[2];
								unsigned char GrayPixel[3] = {(unsigned char)Gray, (unsigned char)Gray, (unsigned char)Gray};
								write(File_Descriptor, GrayPixel, sizeof(GrayPixel));
							}
						}
					}

					exit(Heigth);
				}
			}
			else
			{
				pid_t pid3 = fork();
				Process_Counter++;
				if (pid3 == 0)
				{
					char *File_Name = GetFileName(GetFullPath(argv[2], Name));
					int File_Stat = open(File_Name, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
					if (File_Stat == -1)
					{
						perror("Eroare deschidere fisier");
						exit(-1);
					}

					writeFileName(Name);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeFileSize(File_size);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeUserID(User_id);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeLastModified(Last_modified);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeLinkCounter(Link_cnt);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeUserRights(User_rights);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeGroupRights(Group_rights);
					sr = write(File_Stat, buffer, countChars(buffer));
					writeOtherRights(Other_rights);
					sr = write(File_Stat, buffer, countChars(buffer));
					if (close(File_Stat) < 0)
					{
						perror("Eroare inchidere fisier");
						exit(-1);
					}
					exit(8);
				}
			}
		}

		if (S_ISLNK(lst.st_mode))
		{
			pid_t pid4 = fork();
			Process_Counter++;
			if (pid4 == 0)
			{
				char *File_Name = GetFileName(GetFullPath(argv[2], Name));
				int File_Stat = open(File_Name, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
				if (File_Stat == -1)
				{
					perror("Eroare deschidere link");
					exit(-1);
				}

				writeLinkName(Name);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeLinkSize(Link_size);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeFileSize(File_size);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeUserRights(User_rights);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeGroupRights(Group_rights);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeOtherRights(Other_rights);
				sr = write(File_Stat, buffer, countChars(buffer));
				if (close(File_Stat) < 0)
				{
					perror("Eroare inchidere fisier link");
					exit(-1);
				}
				exit(6);
			}
		}
		if (S_ISDIR(st.st_mode))
		{
			pid_t pid5 = fork();
			Process_Counter++;
			if (pid5 == 0)
			{
				char *File_Name = GetFileName(GetFullPath(argv[2], Name));
				int File_Stat = open(File_Name, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
				if (File_Stat == -1)
				{
					perror("Eroare deschidere director");
					exit(-1);
				}

				writeDirectoryName(Name);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeUserID(User_id);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeFileSize(File_size);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeUserRights(User_rights);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeGroupRights(Group_rights);
				sr = write(File_Stat, buffer, countChars(buffer));
				writeOtherRights(Other_rights);
				sr = write(File_Stat, buffer, countChars(buffer));

				if (close(File_Stat) < 0)
				{
					perror("Eroare inchidere director");
					exit(-1);
				}
				exit(6);
			}
		}
	}
	for (int i = 0; i < Process_Counter; i++)
	{
		w = wait(&status);
		if (WIFEXITED(status))
			printf("S-a incheiat procesul cu pid-ul %d  si codul %d\n ", w, WEXITSTATUS(status));
	}

	if (closedir(Input_Dir) < 0)
	{
		perror("eroare inchidere fisier argument");
		exit(1);
	}

	if (closedir(Output_Dir) < 0)
	{
		perror("eroare inchidere fisier statistica");
		exit(1);
	}
}
