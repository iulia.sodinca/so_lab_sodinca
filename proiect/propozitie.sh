#!/bin/sh

if test "$#" -lt 1
then echo "Usage: $0 <character> \n"

else
count=0
c=$1
while read line
      do
      res=`echo $line | grep -E "$c" | grep -E "^[A-Z][ ]?[a-zA-Z\ ,.!?]*[.!?]$" | grep -E -v "(,[ ]*si)|(si[ ][ ]+)" | grep -E -v "n[bp]"`
	    if [ ! -z "$res" ]
	    then
		count=`expr $count + 1`
	    fi
      done
      echo $count
fi
